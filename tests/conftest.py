import typing
import time

import pytest
import threading
from typing import Optional

from remotecall import Server


@pytest.fixture(scope="module")
def server_address():
    return "localhost", 8000


@pytest.fixture(scope="module")
def server(server_address):
    def foo(a: int, b: str = "foo", c: int = 1, d: bool = True) -> bool:
        """Foo.

        Test docstring.
        """
        return True

    def no_arguments():
        pass

    def optional_arg(a: Optional[str] = None):
        """Optional string argument."""

    def alternative_arg(a: typing.Union[int, float]):
        """Alternative types."""

    def ääkkösiä_nimessä():
        """Non-ASCII characters in name."""

    def ääkkösiä_muuttujina(ä: int, ö: str):
        """Non-ASCII characters in an identifier."""
        pass

    def ääkkösiä_dokumentaatiossa():
        """Non-ASCII characters in documentation string.

        Danish:
            æ ø

        Finnish and Swedish:
            [Ä]
            Å Ä Ö å ä ö

        Icelandic:
            A Á B D Ð E É F G H I Í J K L M N O Ó P R S T U Ú V X Y Ý Þ Æ Ö
        """

    def emojis():
        """😁😛😋
        \U0001F603"""

    def sleep(seconds: float):
        time.sleep(seconds)


    with Server(server_address) as server:
        server.expose(foo)
        server.expose(no_arguments)
        server.expose(optional_arg)
        server.expose(alternative_arg)
        server.expose(ääkkösiä_nimessä)
        server.expose(ääkkösiä_muuttujina)
        server.expose(ääkkösiä_dokumentaatiossa)
        server.expose(emojis)
        server.expose(sleep)

        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        yield server
        server.shutdown()
        server_thread.join()
