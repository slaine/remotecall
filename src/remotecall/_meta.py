__all__ = ["__author__", "__license__", "__version__", "__credits__", "__maintainer__"]
__author__ = "Sami Laine"
__license__ = "BSD-3-Clause"
__version__ = "0.4.0"
__credits__ = []
__maintainer__ = "Sami Laine"
