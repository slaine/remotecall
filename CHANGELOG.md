# Changelog

## 0.4.0

### Added

- Adds functionality to re-raise server-side errors on client-side.
- Improves error handling.

### Fixed

- Fixes issue with generating a client code where default string values were 
  missing quotation marks. 

## 0.3.1

### Fixed

- Fixes an issue where default value of a function parameter was returned 
  incorrectly if the type of the value was Enum.
- Fixes an issue where union type of annotation (typing.Union) could contain duplicates. 

## 0.3.0

### Added

- Adds set_timeout() method to client.
- Improves type handling of optional arguments.

### Changed

- Removes timeout argument from client's call() method.
- Exceptions are located under exceptions module. 

## 0.2.0

### Added

- NamedTupleCoded
- DataClassCodec
- EnumCodec

### Changed

- Improves type annotation support.
- `return_annotation` changed to `returnAnnotation` in API definition.
- Parameter and return type annotations use `list` instead of `str`.

## 0.1.6

### Added

- Client uses requests.Session object for persistent sessions.

## 0.1.5

### Fixed

- UTF-8 content-type is used with HTTP GET response headers.
- UTF-8 encoding is used to write API definition to a file.
- UTF-8 encoding is used to write generated client code to a file.
- Fixes issue with client generation where default value is quoted twice.

## 0.1.4

### Fixed

- ModuleNotFoundError when importing extras.

## 0.1.3

### Added

- NumpyCoded as extra coded.
- ImageCoded as extra codec.
- Long description.

## 0.1.2

### Added

- Improved annotation handling.
- Added unit tests.

### Fixed

- Fixes issue where default value of an optional parameter was missing.

## 0.1.1

### Added

- Added CHANGELOG.md.

### Fixed

- Added missing `constants/__init__.py` 
