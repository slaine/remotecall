from remotecall import Server


def hello() -> str:
    return "Hello World"


with Server(server_address=("localhost", 8000)) as server:
    server.expose(hello)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
