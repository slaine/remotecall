from remotecall import BaseClient


class ExampleClient(BaseClient):

    def hello(self) -> str:
        return self.call("hello")


client = ExampleClient(server_address=("localhost", 8000))
print(client.hello())
