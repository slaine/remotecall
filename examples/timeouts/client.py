import sys

from remotecall import BaseClient
from remotecall.exceptions import ConnectionTimeoutError


client = BaseClient(server_address=("localhost", 8000))

# Set a global timeout to connect and read data.
client.set_timeout(1.0)

# Set function specific default timeout.
client.set_timeout(timeout=1.5, function="sleep_one_second")

# Using 0.1 seconds timeout to connect and 2.5 seconds timeout to read data.
client.set_timeout(timeout=(0.1, 2.5), function="sleep_two_seconds")

print(f"Default timeout is {client.get_timeout()} seconds.")
print("Calling 'sleep_one_second'.")

try:
    client.call("sleep_one_second")
except ConnectionTimeoutError as error:
    print(error)
    sys.exit(2)
except ConnectionError as error:
    print(error)
    sys.exit(1)

print("Calling 'sleep_two_seconds'.")

try:
    client.call("sleep_two_seconds")
except ConnectionError as error:
    print(error)
    sys.exit(2)

print("Done.")
