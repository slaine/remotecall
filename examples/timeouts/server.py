import time

from remotecall import Server


def sleep_one_second():
    time.sleep(1.0)


def sleep_two_seconds():
    time.sleep(2.0)


with Server(server_address=("localhost", 8000)) as server:
    server.expose(sleep_one_second)
    server.expose(sleep_two_seconds)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
