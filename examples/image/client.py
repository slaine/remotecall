from __future__ import annotations

try:
    from PIL import Image
except ImportError as err:
    print("This example requires Pillow (PIL) to be installed.")
    print("Please install Pillow (https://pillow.readthedocs.io/en/stable/):")
    print("    $ python3 -m pip install --upgrade Pillow")
    exit(-1)

from remotecall import BaseClient

# Imported codecs gets automatically registered.
from remotecall.extras.imagecodec import ImageCodec


class ImageClient(BaseClient):
    def create(self, width: int, height: int, color: str) -> Image:
        return self.call("create", width=width, height=height, color=color)

    def draw_text(self, image: Image, text: str) -> Image:
        return self.call("draw_text", image=image, text=text)


client = ImageClient()
image = client.create(width=300, height=400, color="green")
image = client.draw_text(image, "Hello world")
image.show()
