try:
    from PIL import Image
    from PIL import ImageDraw
except ImportError as err:
    print("This example requires Pillow (PIL) to be installed.")
    print("Please install Pillow (https://pillow.readthedocs.io/en/stable/):")
    print("    $ python3 -m pip install --upgrade Pillow")
    exit(-1)

from remotecall import Server

# Imported codecs gets automatically registered.
from remotecall.extras.imagecodec import ImageCodec


def create_image(width: int, height: int, color: str) -> Image.Image:
    return Image.new('RGB', (height, width), color=color)


def draw_text(image: Image.Image, text: str) -> Image.Image:
    d = ImageDraw.Draw(image)
    d.text((10, 10), text, fill=(255, 255, 0))
    return image


with Server(server_address=("localhost", 8000)) as server:
    server.expose(create_image, "create")
    server.expose(draw_text, "draw_text")

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
