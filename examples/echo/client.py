from remotecall import BaseClient


class EchoClient(BaseClient):

    def echo(self, message: str) -> str:
        return self.call("echo", message=message)


client = EchoClient(server_address=("localhost", 8000))
print(client.echo("Hello World"))
