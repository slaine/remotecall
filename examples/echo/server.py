from remotecall import Server


def echo(message: str) -> str:
    return message


with Server(server_address=("localhost", 8000)) as server:
    server.expose(echo)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
