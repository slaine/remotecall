import threading

from remotecall import BaseClient


class Client(BaseClient):

    def work(self, duration: float) -> str:
        return self.call("work", duration=duration)


threads = []
client = Client(server_address=("localhost", 8000))

for _ in range(3):
    thread = threading.Thread(target=client.work, args=(5,))
    threads.append(thread)
    print(f"{thread} created.")


for thread in threads:
    thread.start()
    print(f"{thread} started.")


for thread in threads:
    thread.join()
    print(f"{thread} ready.")
