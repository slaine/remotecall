import time

from remotecall import Server


def long_running_task(duration: float = 3.0):
    time.sleep(duration)


with Server(server_address=("localhost", 8000)) as server:
    server.expose(long_running_task, "work")

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
