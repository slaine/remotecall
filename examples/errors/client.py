from remotecall import BaseClient
from remotecall import ServerError

from server import ExampleError


class Client(BaseClient):

    def raise_system_error(self) -> str:
        return self.call("raise_system_error")

    def raise_value_error(self, value: int) -> str:
        return self.call("raise_value_error", value=value)

    def raise_user_error(self):
        return self.call("raise_user_error")


client = Client(server_address=("localhost", 8000))

# Register a user defined error that may occur while calling a server function.
client.register_exception(ExampleError)

# It is also possible to register an error with a name.
# client.register_error(RuntimeError, "ExampleError")

try:
    client.raise_value_error(42)
except ValueError as error:
    print(f"ValueError: {error}")

try:
    client.raise_user_error()
except ExampleError as error:
    print(f"ExampleError: {error}")
except RuntimeError as error:
    # Raised if ExampleError is registered as RuntimeError.
    print(f"RuntimeError: {error}")
except ServerError as error:
    # ServerError is raised if undefined (unregistered) error occurs.
    print(f"ServerError: {error}")
