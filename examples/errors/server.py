from remotecall import Server


class ExampleError(Exception):
    """Example error."""


def raise_value_error(value: int):
    raise ValueError(f"Invalid value {value}.")


def raise_user_error():
    raise ExampleError("Example error.")


def main():
    with Server(server_address=("localhost", 8000)) as server:
        server.expose(raise_value_error)
        server.expose(raise_user_error)

        try:
            server.serve_forever()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    main()
