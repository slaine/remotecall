"""
This example demonstrates how to generate a Python client for a service. The client code
is generated based on an API definition fetched from a server (that provides the service).

The (missing) client.py can be generated:

    Change the current working directory to calculator example directory:
        $ cd examples/calculator

    Start the server that provides the service:
        $ python server.py

    Fetch the API definition and generate the client:
        $ python -m remotecall generate_client http://localhost:8000 --output client.py

The generated client class is named as 'Client' by default. This can be changed by proving
'name' option for 'generate_client' command.

    $ python -m remotecall generate_client http://localhost:8000 --output client.py --name Calculator

"""

try:
    from client import Client
except ImportError as err:
    print(err)
    print(f"See {__file__} for instructions to generate the missing client.py.")
    exit(-1)

calculator = Client(server_address=("127.0.0.1", 8000))

info = calculator.get_version_info()
print(f"{info['name']} {info['version']}.")
print("4 + 2 =", calculator.add(4, 2))
print("4 - 2 =", calculator.subtract(4, 2))
print("4 * 2 =", calculator.multiply(4, 2))
print("4 / 2 =", calculator.divide(4, 2))
