class Calculator:

    @classmethod
    def add(cls, augend: float, append: float = 0) -> float:
        """Add two numbers.

        For example:
            add(1, 2)
            >> 3
        """
        return augend + append

    @classmethod
    def subtract(cls, minuend: float, subtrahend: float) -> float:
        """Subtract two numbers."""
        return minuend - subtrahend

    @classmethod
    def divide(cls, dividend: float, divisor: float) -> float:
        """Divide two numbers."""
        return dividend / divisor

    @classmethod
    def multiply(cls, multiplier: float, multiplicand: float) -> float:
        """Multiply two numbers."""
        return multiplier * float(multiplicand)

    @classmethod
    def get_version_info(cls) -> dict:
        """Version info."""
        return dict(name="Calculator", version="1.0.0")
