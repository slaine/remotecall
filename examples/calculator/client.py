from __future__ import annotations

from remotecall import BaseClient


class Client(BaseClient):
    def __init__(self, server_address=('localhost', 8000)):
        super().__init__(server_address=server_address)

    def add(self, augend: float, append: float = 0) -> float:
        """Add two numbers.
        
        For example:
            add(1, 2)
            >> 3
        """
        return self.call("add", augend=augend, append=append)

    def subtract(self, minuend: float, subtrahend: float) -> float:
        """Subtract two numbers.
        """
        return self.call("subtract", minuend=minuend, subtrahend=subtrahend)

    def divide(self, dividend: float, divisor: float) -> float:
        """Divide two numbers.
        """
        return self.call("divide", dividend=dividend, divisor=divisor)

    def multiply(self, multiplier: float, multiplicand: float) -> float:
        """Multiply two numbers.
        """
        return self.call("multiply", multiplier=multiplier, multiplicand=multiplicand)

    def get_version_info(self) -> dict:
        return self.call("get_version_info")
