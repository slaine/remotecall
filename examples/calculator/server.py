from remotecall import Server

from calculator import Calculator


with Server(server_address=("127.0.0.1", 8000)) as server:
    server.expose(Calculator.add)
    server.expose(Calculator.subtract)
    server.expose(Calculator.divide)
    server.expose(Calculator.multiply)
    server.expose(Calculator.get_version_info)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
