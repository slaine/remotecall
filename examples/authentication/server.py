from remotecall import Server
from remotecall.authentication import BasicAuthenticator


def hello() -> str:
    return "Hello World"


with Server(server_address=("localhost", 8000)) as server:
    server.set_authenticator(BasicAuthenticator("user", "pass"))
    server.expose(hello)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
