from requests.auth import HTTPBasicAuth

from remotecall import BaseClient


class EchoClient(BaseClient):

    def hello(self) -> str:
        return self.call("hello")


client = EchoClient(server_address=("localhost", 8000))
client.set_authentication(HTTPBasicAuth("user", "pass"))

print(client.hello())
