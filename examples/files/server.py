import logging

from remotecall import Server


logger = logging.getLogger(__name__)


class CloudStorage:
    def __init__(self):
        self._data = {}

    def upload(self, filename: str, data: bytes):
        self._data[filename] = data

    def download(self, filename: str) -> bytes:
        return self._data[filename]


storage = CloudStorage()


with Server(server_address=("localhost", 8000)) as server:
    server.expose(storage.upload)
    server.expose(storage.download)
    server.serve_forever()
