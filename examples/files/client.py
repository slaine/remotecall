from __future__ import annotations

import os

from remotecall.client import BaseClient


class Client(BaseClient):
    def upload(self, filename: str, data: bytes):
        self.call("upload", filename=filename, data=data)

    def download(self, filename: str) -> bytes:
        return self.call("download", filename=filename)


def generate_data_file(filename: str):
    with open(filename, mode="wb") as fh:
        fh.write("First line of example data.\nSecond line of example data.".encode())


example_data_file = "example_data.txt"


if not os.path.exists(example_data_file):
    generate_data_file(example_data_file)


client = Client()


with open(example_data_file, mode="rb") as f:
    client.upload("example", f.read())


with open("download_data.txt", mode="wb") as f:
    f.write(client.download("example"))


