from remotecall import BaseClient

from numpy.typing import NDArray

try:
    from PIL import Image
except ImportError as err:
    print("This example requires Pillow (PIL) to be installed.")
    print("Please install Pillow (https://pillow.readthedocs.io/en/stable/):")
    print("    $ python3 -m pip install --upgrade Pillow")
    exit(-1)


# Imported codecs gets automatically registered.
from remotecall.extras.numpycodec import NumPyCodec


class Client(BaseClient):

    def create(self) -> NDArray:
        return self.call("create")

    def crop(self, array: NDArray, x: int, y: int, width: int, height: int) -> NDArray:
        return self.call("crop", array=array, x=x, y=y, width=width, height=height)


client = Client()
npy_image = client.create()

pil_image = Image.fromarray(npy_image)
pil_image.show()

npy_image = client.crop(npy_image, 0, 0, 20, 20)
Image.fromarray(npy_image).show()
