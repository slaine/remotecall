from remotecall import Server

import numpy as np
from numpy.typing import NDArray

# Imported codecs gets automatically registered.
from remotecall.extras.numpycodec import NumPyCodec


def create_image_array(width: int = 1080, height: int = 1920) -> NDArray:
    """Create NumPy array that represents an image."""
    color = (255, 255, 55)
    array = np.zeros(shape=(width, height, 3), dtype=np.uint8)
    array[:] = color
    return array


def crop_image_array(array: NDArray, x: int, y: int, width: int, height: int) -> NDArray:
    """Crop NumPy array."""
    return array[y:y + height, x:x + width]


with Server(server_address=("localhost", 8000)) as server:
    # It's enough to import NumPyCodec as long as it happens before instantiating the server.
    # If needed, a codec can be registered like "server.codecs.register(NumPyCodec)".
    server.expose(create_image_array, "create")
    server.expose(crop_image_array, "crop")

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
