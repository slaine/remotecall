"""
How to create a self-signed certificate.

    These instructions are based on a blog post made by Alexander Zeitler: "Fixing Chrome 58+
    [missing_subjectAltName] with openssl when using self signed certificates"
    (https://alexanderzeitler.com/articles/Fixing-Chrome).

    Create openssl configuration file ("server.csr.cnf"):

        $ vi server.csr.cnf

            [req]
            default_bits = 2048
            prompt = no
            default_md = sha256
            distinguished_name = dn

            [dn]
            C=US
            ST=New York
            L=Rochester
            O=End Point
            OU=Testing Domain
            emailAddress=your-administrative-address@your-awesome-existing-domain.com
            CN = localhost

    Create X509 v3 configuration to convert from v1 ("v3.ext"):

        $ vi v3.ext

            authorityKeyIdentifier=keyid,issuer
            basicConstraints=CA:FALSE
            keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
            subjectAltName = @alt_names

            [alt_names]
            DNS.1 = localhost

    Create root certificate (Certificate Authority, CA):

        $ openssl genrsa -des3 -out rootCA.key 2048
        $ openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem

            Country Name (2 letter code) []:
            State or Province Name (full name) []:
            Locality Name (eg, city) []:
            Organization Name (eg, company) []:
            Organizational Unit Name (eg, section) []:
            Common Name (eg, fully qualified host name) []:localhost
            Email Address []:

    Create server certificate:

        $ openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config <( cat server.csr.cnf )
        $ openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext

"""

from remotecall import Server


def hello() -> str:
    return "Hello World"


with Server(server_address=("localhost", 8000)) as server:
    server.use_ssl(cert_file="server.crt", key_file="server.key")
    server.expose(hello)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
