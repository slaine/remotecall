from remotecall import BaseClient


class Client(BaseClient):

    def hello(self) -> str:
        return self.call("hello")


client = Client(server_address=("localhost", 8000))
client.use_ssl(cert_file="rootCA.pem")

print(client.hello())
